**FOR THE MOBILE APP, USE THE MOBILE BRANCH **

When developing a mobile application,
I ran into the problem that local storage
is not defined in a mobile environment.
This repository is also used in the web
version of the application, so the best
way was to create a branch separately
for the mobile platform so as not to harm
the web version.