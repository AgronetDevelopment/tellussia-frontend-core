import ApiRequest from "./ApiRequest";

class SettingsAdvisedCompaniesService {

    static async getAdvisedCompanies(mobileData) {
        const data = await ApiRequest.apiRequest({method: 'GET', url: 'adviser_to_consumers/all', ...mobileData});
        return data;
    }

    static async inviteCompanyUser(obj, mobileData) {
        const data = await ApiRequest.apiRequest({method: 'POST', url: `adviser_to_consumers/invite_user/${obj.email}`, ...mobileData});
        return data;
    }

}

export default SettingsAdvisedCompaniesService
