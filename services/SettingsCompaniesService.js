import ApiRequest from "./ApiRequest";

class SettingsCompaniesService {

    static async getCompanies(mobileData) {
        const data = await ApiRequest.apiRequest({method: 'GET', url: 'company/', ...mobileData});
        return data;
    }

    static async getCompanyTypes(mobileData) {
        const data = await ApiRequest.apiRequest({method: 'GET', url: 'company/types', ...mobileData});
        return data;
    }

    static async addCompany(obj, mobileData) {
        const data = await ApiRequest.apiRequest({method: 'POST', url: 'company/create', body: obj, ...mobileData});
        return data;
    }

    static async editCompany(obj, mobileData) {
        const data = await ApiRequest.apiRequest({method: 'POST', url: 'company/update', body: obj, ...mobileData});
        return data;
    }

    static async editCompanyUser(obj, mobileData) {
        const data = await ApiRequest.apiRequest({method: 'POST', url: 'users/panel_update', body: obj, ...mobileData});
        return data;
    }

    static async addCompanyUser(obj, mobileData) {
        const data = await ApiRequest.apiRequest({method: 'POST', url: 'registration/', body: obj, ...mobileData});
        return data;
    }
}

export default SettingsCompaniesService
