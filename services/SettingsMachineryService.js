import ApiRequest from "./ApiRequest";

class SettingsMachineryService {

  static async getTractors(mobileData) {
    const data = await ApiRequest.apiRequest({method: 'GET', url: 'machinery/tractors/all', ...mobileData});
    return data;
  }

  static async getTrailers(mobileData) {
    const data = await ApiRequest.apiRequest({method: 'GET', url: 'machinery/trailers/all', ...mobileData});
    return data;
  }

  static async getUsersForMachinery(mobileData) {
    const data = await ApiRequest.apiRequest({method: 'GET', url: 'machinery/users_for_machinery', ...mobileData});
    return data;
  }

  static async createTractor(obj, mobileData) {
    const data = await ApiRequest.apiRequest({method: 'POST', url: 'machinery/tractors/create', body: obj, ...mobileData});
    return data;
  }

  static async updateTractor(obj, mobileData) {
    const data = await ApiRequest.apiRequest({method: 'POST', url: 'machinery/tractors/update', body: obj, ...mobileData});
    return data;
  }

  static async createTrailer(obj, mobileData) {
    const data = await ApiRequest.apiRequest({method: 'POST', url: 'machinery/trailers/create', body: obj, ...mobileData});
    return data;
  }

  static async updateTrailer(obj, mobileData) {
    const data = await ApiRequest.apiRequest({method: 'POST', url: 'machinery/trailers/update', body: obj, ...mobileData});
    return data;
  }

  static async attachFile(formData, id, mobileData) {
    const data = await ApiRequest.apiRequest({
      method: 'POST',
      url: `machinery/trailers/file_upload/${id}`,
      body: formData,
      isFormData: true,
      ...mobileData
    });
    return data;
  }

}

export default SettingsMachineryService
