import ApiRequest from "./ApiRequest";

class SettingsCropManageService {
    static async getCropTypes(mobileData) {
        const data = await ApiRequest.apiRequest({method: 'GET', url: 'crop_type/all', ...mobileData});
        return data;
    }

    static async createCropType(obj, mobileData) {
        const data = await ApiRequest.apiRequest({method: 'POST', url: 'crop_type/add_crop', body: obj, ...mobileData});
        return data;
    }

    static async createVariety(obj, mobileData) {
        const data = await ApiRequest.apiRequest({method: 'POST', url: 'crop_type/add_variety', body: obj, ...mobileData});
        return data;
    }
}

export default SettingsCropManageService
