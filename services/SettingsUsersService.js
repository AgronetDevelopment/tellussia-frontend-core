import ApiRequest from "./ApiRequest";

class SettingsUsersService {

    static async getUsers(mobileData) {
        const data = await ApiRequest.apiRequest({method: 'GET', url: 'users/', ...mobileData});
        return data;
    }

    static async getRoles(mobileData) {
        const data = await ApiRequest.apiRequest({method: 'GET', url: 'registration/roles', ...mobileData});
        return data;
    }

    static async getFilteredUsers(obj, mobileData) {
        const data = await ApiRequest.apiRequest({method: 'POST', url: 'users/filtered', body: obj, ...mobileData});
        return data;
    }
}

export default SettingsUsersService
