import ApiRequest from "./ApiRequest";

class TechnicalAdviceService {

  static async getTechnicalAdvicesTypes(mobileData) {
    const data = await ApiRequest.apiRequest({method: 'GET', url: 'technical_advice/advice_types', ...mobileData});
    return data;
  }

  static async getAdviceCompanies(mobileData) {
    const data = await ApiRequest.apiRequest({method: 'GET', url: 'technical_advice/adviced_companies', ...mobileData});
    return data;
  }

  static async getUnits(mobileData) {
    const data = await ApiRequest.apiRequest({method: 'GET', url: 'technical_advice/chemical_units', ...mobileData});
    return data;
  }

  static async getTechnicalAdvice(obj, mobileData) {
    const data = await ApiRequest.apiRequest({method: 'POST', url: 'technical_advice/filtered', body: obj, ...mobileData});
    return data;
  }

  static async assignCompanies(obj, mobileData) {
    const data = await ApiRequest.apiRequest({method: 'POST', url: 'technical_advice/send_to_recipients', body: obj, ...mobileData});
    return data;
  }

  static async createTechnicalAdvice(obj, mobileData) {
    const body = {
      ...obj,
      chemicals: obj.chemicals.map(el => {
        return {name: el.name, dosage: el.dosage, unit: el.unit}
      })
    }

    const data = await ApiRequest.apiRequest({method: 'POST', url: 'technical_advice/add', body, ...mobileData});
    return data;
  }

  static async attachTechnicalAdviceFile(formData, id, mobileData) {
    const data = await ApiRequest.apiRequest(
      {method: 'POST', url: `technical_advice/file_upload/${id}`, body: formData, isFormData: true, ...mobileData}
    );
    return data;
  }

  static async updateTechnicalAdviceStatus(obj, mobileData) {
    const data = await ApiRequest.apiRequest({method: 'POST', url: 'technical_advice/update_status', body: obj, ...mobileData});
    return data;
  }
}

export default TechnicalAdviceService
