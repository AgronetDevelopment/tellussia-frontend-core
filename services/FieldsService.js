import ApiRequest from "./ApiRequest";

class FieldsService {

  static async getFields(mobileData) {
    const data = await ApiRequest.apiRequest({method: 'GET', url: 'myfields/paged'}, ...mobileData);
    return data;
  }

  static async filterFields(obj, mobileData) {
    const data = await ApiRequest.apiRequest({method: 'POST', url: 'myfields/filtered', body: obj, ...mobileData});
    return data;
  }

  static async validateFieldName(name, mobileData) {
    const body = {name};
    const data = await ApiRequest.apiRequest({method: 'POST', url: 'myfields/validate_name', body, responseType: 'text', ...mobileData});
    return data;
  }

  static async createField(obj, mobileData) {
    const body = {...obj, surface: Number(obj.surface)};
    const data = await ApiRequest.apiRequest({method: 'POST', url: 'myfields/add', body, ...mobileData});
    return data;
  }

  static async updateField(obj, mobileData) {
    const body = {...obj, surface: Number(obj.surface)};
    const data = await ApiRequest.apiRequest({method: 'POST', url: 'myfields/update', body, ...mobileData});
    return data;
  }

  static async deleteField(id, mobileData) {
    const data = await ApiRequest.apiRequest({method: 'DELETE', url: `myfields/delete/${id}`, ...mobileData});
    return data;
  }

  static async validateCustomCode(shortCode, mobileData) {
    const body = {shortCode};
    const data = await ApiRequest.apiRequest({method: 'POST', url: 'myfields/validate_short_code', body, responseType: 'text', ...mobileData});
    return data;
  }

}

export default FieldsService;
