import ApiRequest from "./ApiRequest";

class CropsService {

    static async getCropTypes(mobileData) {
        const data = await ApiRequest.apiRequest({method: 'GET', url: 'crop_type/all', ...mobileData});
        return data;
    }

    static async validateSurface(obj, mobileData) {
        const data = await ApiRequest.apiRequest({method: 'POST', url: 'crops/validate_surface', body: obj, ...mobileData});
        return data;
    }

    static async validateSurfaceCreation(obj, mobileData) {
        const data = await ApiRequest.apiRequest({method: 'POST', url: 'crops/validate_surface_with_message', body: obj, ...mobileData});
        return data;
    }

    static async validateSurfaceAndCropCode(obj, mobileData) {
        const data = await ApiRequest.apiRequest({method: 'POST', url: 'crops/validate_crop_surface_and_code', body: obj, ...mobileData});
        return data;
    }

    static async validateSurfaceUpdate(obj, mobileData) {
        const data = await ApiRequest.apiRequest({method: 'POST', url: 'crops/validate_surface_change_with_message', body: obj, ...mobileData});
        return data;
    }

    static async createCrop(obj, mobileData) {
        const body = {...obj, cropTypeId: Number(obj.cropTypeId), varietyId: Number(obj.varietyId), surface: Number(obj.surface), ...mobileData};
        const data = await ApiRequest.apiRequest({method: 'POST', url: 'crops/add', body});
        return data;
    }

    static async updateCropDate(id, date, mobileData) {
        const body = {id, dateTo: date};
        const data = await ApiRequest.apiRequest({method: 'POST', url: 'crops/update', body, ...mobileData});
        return data;
    }

    static async deleteCrop(id, mobileData) {
        const data = await ApiRequest.apiRequest({method: 'DELETE', url: `crops/delete/${id}`, ...mobileData});
        return data;
    }

}

export default CropsService;
