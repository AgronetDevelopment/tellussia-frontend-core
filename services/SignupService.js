import ApiRequest from "./ApiRequest";


class SignupService {

    static async signUp(obj, mobileData) {
        const data = await ApiRequest.apiRequest({method: 'POST', url: 'auth/signUp', body: obj, responseType: 'status', ...mobileData});
        return data;
    }

    static async confirmPassword(token, password, mobileData) {
        const data = await ApiRequest.apiRequest({method: 'POST', url: `auth/signUp/${token}/confirm?password=${password}`, ...mobileData});
        if (!!data.token) {
            sessionStorage.setItem('tellussiaToken', 'Bearer ' + data.token);
        }
        return data;
    }

    static async createCompany(obj, mobileData) {
        const data = await ApiRequest.apiRequest({method: 'POST', url: 'signUp/company', body: obj, ...mobileData});
        return data;
    }

}

export default SignupService
