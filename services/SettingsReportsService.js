import ApiRequest from "./ApiRequest";

class SettingsReportsService {

    // static async filterReports(obj, mobileData) {
    //     const data = await ApiRequest.apiRequest({method: 'POST', url: 'external_reports/filtered', body: obj, ...mobileData});
    //     return data;
    // }

    static async filterReports(obj, mobileData) {
        const data = await ApiRequest.apiRequest({method: 'POST', url: 'external_reports/kpi_filtered', body: obj, ...mobileData});
        return data;
    }

    static async attachFile(formData, mobileData) {
        const data = await ApiRequest.apiRequest({
            method: 'POST',
            url: `external_reports/kpi_file_upload`,
            body: formData,
            isFormData: true,
            ...mobileData
        });
        return data;
    }

    static async deleteReport(obj, mobileData) {
        const data = await ApiRequest.apiRequest({method: 'DELETE', url: `external_reports/kpi_delete/${obj.id}/${obj.reportType}`, ...mobileData});
        return data;
    }

}

export default SettingsReportsService
