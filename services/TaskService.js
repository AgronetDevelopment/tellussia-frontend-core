import ApiRequest from "./ApiRequest";

class TaskService {

    static async filterTasks(obj, mobileData) {
        const data = await ApiRequest.apiRequest({method: 'POST', url: 'fieldbook/tasks_filtered', body: obj, ...mobileData});
        return data;
    }

    static async getTaskDetails(id, mobileData) {
        const data = await ApiRequest.apiRequest({method: 'GET', url: `fieldbook/tasks_details/${id}`, ...mobileData});
        return data;
    }

    static async getCropsForTask(mobileData) {
        const data = await ApiRequest.apiRequest({method: 'GET', url: 'fieldbook_utils/crops_for_task', ...mobileData});
        return data;
    }

    static async getTractorsForTask(mobileData) {
        const data = await ApiRequest.apiRequest({method: 'GET', url: 'fieldbook_utils/tractors', ...mobileData});
        return data;
    }

    static async getTrailersForTask(mobileData) {
        const data = await ApiRequest.apiRequest({method: 'GET', url: 'fieldbook_utils/trailers', ...mobileData});
        return data;
    }

    static async getUsersForTask(mobileData) {
        const data = await ApiRequest.apiRequest({method: 'GET', url: 'fieldbook_utils/users_for_task', ...mobileData});
        return data;
    }

    static async getWindDirection(mobileData) {
        const data = await ApiRequest.apiRequest({method: 'GET', url: 'fieldbook_utils/wind_direction_for_task', ...mobileData});
        return data;
    }

    static async getWindStrength(mobileData) {
        const data = await ApiRequest.apiRequest({method: 'GET', url: 'fieldbook_utils/wind_strength_for_task', ...mobileData});
        return data;
    }

    static async getRainSeverity(mobileData) {
        const data = await ApiRequest.apiRequest({method: 'GET', url: 'fieldbook_utils/rain_severity_for_task', ...mobileData});
        return data;
    }

    static async createTask(obj, selectedCrops, mobileData) {
        const body = {
            title: obj.title,
            comment: obj.comment,
            hoursSpent: obj.hoursSpent,
            timeStart: obj.timeStart,
            userId: obj.operator.id,
            cropId: selectedCrops[0].id,
            machinery: {tractorId: obj.tractor.id, trailerId: obj.trailer.id},
            chemicals: obj.chemicals.map(el => { return {name: el.name, dosage: el.dosage, unit: el.unit} })
        };
        const data = await ApiRequest.apiRequest({method: 'POST', url: 'fieldbook/createTask', body, ...mobileData});
        return data;
    }

    static async setTaskInProgress(obj, mobileData) {
        const body = {reason : ''}
        const data = await ApiRequest.apiRequest({method: 'POST', url: `tasks/${obj.id}/doing?reason`, body: body, ...mobileData});
        return data;
    }

    static async setTaskDone(obj, mobileData) {
        const body = {reason : ''}
        const data = await ApiRequest.apiRequest({method: 'POST', url: `tasks/${obj.id}/complete?reason`, body: body, ...mobileData});
        return data;
    }

    static async setTaskClosed(obj, mobileData) {
        const body = {reason : ''}
        const data = await ApiRequest.apiRequest({method: 'POST', url: `tasks/${obj.id}/close?reason`, body: body, ...mobileData});
        return data;
    }

}

export default TaskService
