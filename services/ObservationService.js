import ApiRequest from "./ApiRequest";

class ObservationService {

  static async getObservationTypes(mobileData) {
    const data = await ApiRequest.apiRequest({method: 'GET', url: 'observation/observation_types', ...mobileData});
    return data;
  }

  static async getObservations(obj, mobileData) {
    const data = await ApiRequest.apiRequest({method: 'POST', url: 'observation/filtered', body: obj, ...mobileData});
    return data;
  }

  static async updateObservationStatus(obj, mobileData) {
    const data = await ApiRequest.apiRequest({method: 'POST', url: 'observation/update_status', body: obj, ...mobileData});
    return data;
  }

  static async addObservation(formData, mobileData) {
    const data = await ApiRequest.apiRequest({method: 'POST', url: 'observation/add', body: formData, isFormData: true, ...mobileData});
    return data;
  }
}

export default ObservationService;
