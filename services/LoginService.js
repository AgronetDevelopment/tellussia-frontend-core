import ApiRequest from "./ApiRequest";

class LoginService {

  static async logIn(payload, baseUrl, storeToken) {
    const data = await ApiRequest.apiRequest({method: 'POST', url: 'auth/single_step_login/initial_login', body: payload, noAuth: true, baseUrl});
    if (!!data.token && !baseUrl) {
      sessionStorage.setItem('tellussiaToken', 'Bearer ' + data.token);
    } else if (baseUrl) {
      const session = {
        accessToken: data.token
      }
      storeToken(session);
    }
    return data;
  }

  static async finalStepLogin(payload, baseUrl, token, storeToken) {
    const data = await ApiRequest.apiRequest({method: 'POST', url: 'single_step_login/final_choice', body: payload, token, baseUrl});
    if (!!data.token && !baseUrl) {
      sessionStorage.setItem('tellussiaToken', 'Bearer ' + data.token);
      localStorage.setItem('tellussiaRefreshToken', data.refreshToken);
    } else if (baseUrl) {
      const session = {
        accessToken: data.token,
        refreshToken: data.refreshToken,
      }
      storeToken(session);
    }
    return data;
  }

  static async refreshToken(refreshToken, baseUrl, storeToken) {
    const body = {refreshToken: !refreshToken ? localStorage.getItem('tellussiaRefreshToken') : refreshToken};
    const data = await ApiRequest.apiRequest({method: 'POST', url: 'auth/refreshToken', body, baseUrl});
    console.log('DATA FROM API CALL', data);
    if (!!data.token && !baseUrl) {
      sessionStorage.setItem('tellussiaToken', 'Bearer ' + data.token)
    } else if (baseUrl) {
      const session = {
        accessToken: data.token
      }
      storeToken(session);
    }
    return data;
  }

  static async getUserInfo(mobileData) {
    const data = await ApiRequest.apiRequest({url: "profile/me", method: "GET", ...mobileData});
    return data;
  }

  static async getRegions() {
    const data = await ApiRequest.apiRequest({method: 'GET', url: 'agro_regions/all'});
    return data;
  }

  static async confirmPassword(token, password) {
    const data = await ApiRequest.apiRequest({method: 'PUT', url: `auth/password/confirm/${token}?password=${password}`});
    return data
  }

}

export default LoginService;
