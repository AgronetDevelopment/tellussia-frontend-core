class ApiRequest {

  static async apiRequest({method, url, body, params, isFormData, responseType, noAuth, token, baseUrl}) {
    const paramsString = params && Object.keys(params).length
      ? `?${Object.keys(params).map(param => param + '=' + params[param]).join('&')}`
      : '';

    let headers = !isFormData ? {'Content-Type': 'application/json'} : {};
    if (!noAuth) {
      headers = {...headers,
        'Authorization': !token ? sessionStorage.getItem('tellussiaToken') : token,
        'Local': localStorage.getItem('lang') ? localStorage.getItem('lang') : 'en'
      };
    }

    let result = null;

    try {
      const res = await fetch(`${!baseUrl ? process.env.REACT_APP_API_URL + "/api" : baseUrl}/${url}${paramsString}`, {
        method,
        headers,
        body: !isFormData
          ? JSON.stringify(body)
          : body
      })

      if (res.status === 200) {
        result = responseType === 'text' ? await res.text() : await res.json();
      } else {
        result = responseType === 'text' ? '' : [];
      }

    } catch (e) {
      console.log(e)
    }

    return result;
  }
}

export default ApiRequest;
